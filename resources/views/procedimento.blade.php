@extends ('templates.base')

@section('conteudo')

    <main>
        <h1>Procedimentos</h1>
        <h2>Materiais:</h2>
        <p>Multímetro</p>
        <p>Resistência de 23.8 ohms</p>
        <h2>O que foi feito?</h2>
        <p>Primeiramente, medimos o valor da resistência. Após isto, medimos o valor da tensão sem carga, medindo utilizando o multímetro diretamente nas pilhas e baterias.</p>
        <p>Após isto, medimos o valor da tensão com carga, pegando o multímetro encostando com a ponta da resistência e do outro lado, ligando diretamente na pilha.</p>
        <p>Os valores da resitência de carga(ohm) e da 	Resistência interna(ohm) foram calculados por meio do javascript</p>
        <h2>Imagens das baterias e pilhas medidas:</h2>
        <img class="bateria1" src="../imgs/bateriagolite.jpg" alt="Bateria Golite" height="400" width="400">
        <img class="bateria2" src="../imgs/pilhaluatek.jpg" alt="Bateria Luatek" height="400" width="400">
        <img class="pilha1" src="../imgs/duracellAA.jpg" alt="Pilha Duracell AA" height="400" width="400">
        <img class="pilha2" src="../imgs/duracellAAA.jpg" alt="Pilha Duracell AAA" height="400" width="400">
        <img class="pilha3" src="../imgs/panassonicAA.jpg" alt="Pilha Panassonic AA" height="400" width="400">
        <img class="pilha4" src="../imgs/JVX.jpg" alt="Pilha JVX" height="400" width="400">
        <img class="bateria3" src="../imgs/bateriaelgin.jpg" alt="Bateria Elgin" height="400" width="400">
        <img class="bateria4" src="../imgs/philipsAAA.jpg" alt="Pilha Philips AAA" height="400" width="400">
        <img class="bateria5" src="../imgs/bateriafreedom.jpg" alt="Bateria Freedom" height="400" width="400">
        <img class="bateria6" src="../imgs/bateriaunipower.jpg" alt="Bateria unipower" height="400" width="400">
    </main>
    @section('rodape')
    <h4>Rodape da página principal</h4>
    @endsection