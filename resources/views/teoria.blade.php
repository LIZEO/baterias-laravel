
    @extends ('templates.base')

    @section('conteudo')
    
    <main>
        <h1>Teoria</h1>
        <hr> Determinar a resistência interna de uma fonte de tensão real</hr>
        <p>Uma fonte de tensão real é constituída de uma fonte de tensão ideal (E) em série com a sua resistência interna (r) como mostra o circuito abaixo:</p>
        <img src="../imgs/fonte-real (1).png" alt="fonte de tensão com resistência interna">
        <p>Para conhecermos o valor da tensão interna da fonte (E) basta medirmos diretamente essa fonte com um multímetro desde que não haja nenhuma carga ligada a essa fonte:</p>
        <img src="../imgs/fonte-real 2.png" alt="queda de tensão">
        <p>Quando conectamos uma carga (R) a essa fonte, circulará uma corrente (I) que produzirá queda de tensão interna (Vr) e a queda de tensão externa (VR):</p>
        <img src="../imgs/f2.png" alt="queda de tensão interna e queda de tensão externa">
        <p></p>Através da equação do divisor de tensão temos que:</p>
        <img src="../imgs/fonte-real 2.png" alt="equação do divisor de tensão">
        <p>Passando o termo "r+R" multiplicando à esquerda e o termo "VR" dividindo à direita fica:</p>
        <img src="../imgs/CodeCogsEqn.png" alt="equação do divisor após resolução parcial">
        
        <p>Isolando "r" temos que:</p>
        <img src="../imgs/CodeCogsEqn (2).png" alt="equação após isolar o 'R'">
        <p>Colocando "R" em evidência fica:</p>
        <img src="../imgs/CodeCogsEqn (3).png" alt="equação com o 'R' em evidência">
        <p>Dessa forma podemos medir, indiretamente, a resistência interna de uma pilha ou de uma bateria, através da medição de sua tensão interna (sem carga) "E", da tensão externa (com carga) "VR" e da própria resistência externa "R" e  aplicando a equação da resistência interna "r"!</p>
        </p>
    
    </main>
   
    @section('rodape')
    <h4>Rodape da página principal</h4>
    @endsection